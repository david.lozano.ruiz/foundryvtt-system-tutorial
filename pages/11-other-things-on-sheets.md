# Other things to do on Actor sheets

## Creating rolls that require user input

## Creating tabs to manage multiple kinds of items

## Creating item-like data structures

## Querying for entities with filter

Query for an actor by name.

```js
let name = "My Actor";
let actors = game.actors.entities.filter(a => a.data.name == name);
```

Query for multiple actors by name.

```js
let names = [
    "My Actor",
    "My Other Actor"
];
let actors = game.actors.entities.filter(a => names.includes(a.data.name));
```

## Outputting a list of actors with map

```js
let names = game.actors.entities.map(a => return a.data.name).join(', ');
```

## Modify a sheet or applications markup in JS

In your ActorSheet, ItemSheet, or application class, override the _renderInner() method.

```js
async _renderInner(data, options) {
  const html = await  super._renderInner(data, options);
  // Modify the HTML by adding your stuff here
  return html;
}
```

## Targets

```js
for (let target of game.user.targets) {
  if (target.actor.data.data.attributes.hp.value !== target.actor.data.data.attributes.hp.max) {
    let newHP = target.actor.data.data.attributes.hp.value - 10;
    await target.actor.update({"data.attributes.hp.value": newHP})
  }
}
```

## When in doubt, go look at other systems


- **Next:** []()
- **Prev:** []()
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTkxOTkzMDk4MSwtMTU3OTkwNzk5MywxMT
k4MTIzNDUsLTU3MjMyMDcxOSw4NjIyMTMzNDMsNzYxMzc0NDk3
LDEzNjU0NjYyNCw3NDMzMjE5NTMsMTQxMjg1MDUzLC0zNjQwMT
YyMTRdfQ==
-->