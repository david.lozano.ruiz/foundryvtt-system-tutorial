# Extending the Item class

You can extend the Item class to use your own version, just like we did earlier with the Actor class. Let's start by taking a look a the BoilerplateItem class in `/module/item/item.js`. As with previous examples, you'll want to rename `BoilerplateItem` to whatever your system's name is, such as `MySystemNameItem`.

```js
/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class BoilerplateItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    // Get the Item's data
    const itemData = this.data;
    const actorData = this.actor ? this.actor.data : {};
    const data = itemData.data;
  }
}
```

The Boilerplate system doesn't actually include any other modifications to its Item class, but you could add derived data here, as we did in the Actor class. In addition, you can define methods that can be called when needed, such as a `toRoll()` method to make a roll from an item and send it to chat. We'll revisit that idea later when we add Macrobar support to items.

- **Prev:** [Creating HTML templates for your actor sheets](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/08-creating-html-templates.md)
- **Next:** [Extending the ItemSheet class](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/10-extending-item-sheet-class.md)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTcyNjk1MDQyMiwtMjEyODIwMzE1MiwtNj
MyMDUzOTE4LC01MzE0OTc5MDQsMTA3ODQ4ODgyOV19
-->