# System Tutorial - Start to Finish

Welcome to my system development tutorial for FoundryVTT! My goal is to guide you through system development with little to no knowledge of Foundry or the languages it uses. At first we'll walk through the steps to create relatively simple systems that allow you to collect data for things like stats and attributes and calculate modifiers for them, but eventually we'll get into more advanced topics like making dice rolls from your sheet or letting items be converted into macros.

If you have any questions, you can find me on the Foundry discord as @asacolips#1867. And with that, let's move onto to the tutorial!

- [Getting an empty system together](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/01-getting-started.md)
	- Start with a boilerplate system
	- Using SWS
	- Using a CLI
- [Stuff to be aware of](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/02-stuff-to-be-aware-of.md)
	- Scripts or ES6 modules?
	- CSS preprocessors
	- Handlebars
	- What about 3rd party libraries?
	- Localization
- [system.json](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/03-system.json.md)
- [template.json](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/04-template.json.md)
	- Defining actor and item types
- [Creating your main system javascript file](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/05-creating-main-javascript.md)
- [Extending the Actor class](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/06-extending-actor-class.md)
	- Basic data vs. derived data
	- Creating derived values with prepareData()
	- _prepareCharacterData()
- [Extending the ActorSheet class](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/07-extending-actor-sheet-class.md)
	- defaultOptions
	- getData()
	- activateListeners()
- [Creating HTML templates for your actor sheets](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/08-creating-html-templates.md)
- [Extending the Item class](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/09-extending-item-class.md)
- [Extending the ItemSheet class](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/10-extending-item-sheet-class.md)
- Other things to do on actor sheets
	- [Creating rollable buttons with event listeners](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/11.1-rollable.md)
	- [Grouping items by type](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/11.3-grouping-items.md)
	- [Separating item types into tabs](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/11.4-item-type-tabs.md)
	- [Adding macrobar support for your items](https://gitlab.com/asacolips-projects/foundry-mods/foundryvtt-system-tutorial/-/blob/master/pages/16-macrobar-support.md)

## Sections that are Work-in-Progress

In addition to the above, there are several more sections that will be added to this tutorial over time! Check back in every few weeks to see if any additions have been made. If there's a topic related to system development that you would like to see added to this list, shoot me a message on Discord!

- Other things to do on actor sheets
	- Creating rollable buttons with dialogs
	- Creating item-like data structures
	- Querying for entities with map and filter
	- When in doubt, look at other systems
- CONFIG and things to do with it
- More Localization
- TabsV2
- Flags
- System Settings
- Making things draggable in your sheet
- Overriding core behaviors (like the combat tracker)
- Development patterns to be aware of in the API
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzMyMTM4OSwtMjAxOTUyNzIyLDE0ODkwNT
MzNDQsMTcyMDgzMjAxMywtOTExNTQ0MTM0LDEwODE0ODc0NDEs
LTMxOTEwNTAsLTIwNzA0MTg1NDgsLTg2MjE4OTA5NiwxNTgwMT
U1MDY4LC0xNTMwNDg1MzA2LDEwNTcwMTgwNDcsLTMzOTU2ODc4
MCwtMzM5NTY4NzgwXX0=
-->